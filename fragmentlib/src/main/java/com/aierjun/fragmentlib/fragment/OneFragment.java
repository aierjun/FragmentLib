package com.aierjun.fragmentlib.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.aierjun.fragmentlib.R;


/**
 * Created by Administrator on 2017/6/9 0009.
 */

public class OneFragment extends Fragment {
    private Button button;

    public static  OneFragment self;
    public static synchronized OneFragment getInstance(){
        if (self==null) self=new OneFragment();
        return self;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.fg_one,null);
        Log.d("Ani","fg_one");
        return view;
    }
}
