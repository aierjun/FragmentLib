package com.aierjun.fragmentlib.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aierjun.fragmentlib.R;

/**
 * Created by Administrator on 2017/6/9 0009.
 */

public class FourFragment extends Fragment {

    public static FourFragment self;
    public static synchronized FourFragment getInstance(){
        if (self==null) self=new FourFragment();
        return self;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.fg_four,null);
        return view;
    }
}
