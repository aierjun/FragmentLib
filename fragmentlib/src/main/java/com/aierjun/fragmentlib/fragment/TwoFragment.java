package com.aierjun.fragmentlib.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aierjun.fragmentlib.R;


/**
 * Created by Administrator on 2017/6/9 0009.
 */

public class TwoFragment extends Fragment {

    public static TwoFragment self;
    public static synchronized TwoFragment getInstance(){
        if (self==null) self=new TwoFragment();
        return self;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.fg_two,null);
        return view;
    }
}
