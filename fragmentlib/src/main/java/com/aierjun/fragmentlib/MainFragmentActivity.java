package com.aierjun.fragmentlib;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.aierjun.fragmentlib.fragment.FourFragment;
import com.aierjun.fragmentlib.fragment.OneFragment;
import com.aierjun.fragmentlib.fragment.ThreeFragment;
import com.aierjun.fragmentlib.fragment.TwoFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ani_aierJun on 2018/3/5.
 */

public class MainFragmentActivity extends FragmentActivity {
    private FragmentUtil fragmentUtil;
    private RelativeLayout one_btn;
    private RelativeLayout two_btn;
    private RelativeLayout three_btn;

    private List<Fragment> mFragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmentactivity_main);
        findView();
        initView();
    }

    private void findView() {
        mFragments.add(OneFragment.getInstance());
        mFragments.add(TwoFragment.getInstance());
        mFragments.add(ThreeFragment.getInstance());

        one_btn = findViewById(R.id.one_btn);
        two_btn = findViewById(R.id.two_btn);
        three_btn = findViewById(R.id.three_btn);

    }

    private void initView() {
        fragmentUtil = new FragmentUtil(this, R.id.flMainContent);
        fragmentUtil.initFragments(mFragments);
        fragmentUtil.show(0);

        one_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentUtil.show(0);
            }
        });
        two_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentUtil.show(1);
            }
        });
        three_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentUtil.show(2);
            }
        });
    }
}
